// <auto-generated />
namespace Osvita.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class type_doc : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(type_doc));
        
        string IMigrationMetadata.Id
        {
            get { return "201709040427269_type_doc"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
