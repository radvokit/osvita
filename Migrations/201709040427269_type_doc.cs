namespace Osvita.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class type_doc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Docs", "Type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Docs", "Type");
        }
    }
}
