namespace Osvita.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class number : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Docs", "Number", c => c.String());
            AddColumn("dbo.News", "FBShare", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.News", "FBShare");
            DropColumn("dbo.Docs", "Number");
        }
    }
}
