namespace Osvita.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImageUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "ImageUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.News", "ImageUrl");
        }
    }
}
