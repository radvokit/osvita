namespace Osvita.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class news_docs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Docs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Url = c.String(),
                        Date = c.DateTime(nullable: false),
                        Category = c.String(),
                        Published = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Author = c.String(),
                        ShortText = c.String(),
                        Text = c.String(),
                        UrlSlug = c.String(),
                        Date = c.DateTime(nullable: false),
                        Category = c.String(),
                        Published = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.News");
            DropTable("dbo.Docs");
        }
    }
}
