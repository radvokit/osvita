namespace Osvita.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_image : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.News", "Image");
        }
    }
}
