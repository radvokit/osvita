﻿using Osvita.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Osvita.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            IEnumerable<New> news = db.News;
            IEnumerable<Doc> docs = db.Docs;
            ViewBag.News = news;
            ViewBag.Docs = docs;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "На цій сторінці Ви можете дізнатись основну інформацію стосовно нашої діяльності.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Наші контактні дані.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}