﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Osvita.Startup))]
namespace Osvita
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
