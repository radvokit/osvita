﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osvita.Models
{
    public class Doc
    {
        // ID
        public int Id { get; set; }
        // Номер
        public string Number { get; set; }
        // Заголовок
        public string Title { get; set; }
        // Адреса
        public string Url { get; set; }
        // Дата
        public DateTime Date { get; set; }
        // Категорія
        public string Category { get; set; }
        // Категорія
        public string Type { get; set; }
        // Стан публікації
        public bool Published { get; set; }
    }
}