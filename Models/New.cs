﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osvita.Models
{
    public class New
    {
        // ID
        public int Id { get; set; }
        // Заголовок
        public string Title { get; set; }
        // Автор
        public string Author { get; set; }
        // Короткий текст
        public string ShortText { get; set; }
        // Повний текст
        public string Text { get; set; }
        // Дружня адреса
        public string UrlSlug { get; set; }
        // Дата
        public DateTime Date { get; set; }
        // Категорія
        public string Category { get; set; }
        // Стан публікації
        public bool Published { get; set; }
        // Зображення
        public byte[] Image { get; set; }
        // Посилання на зображення
        public string ImageUrl { get; set; }
        // Facebook поширення
        public int FBShare { get; set; }
    }
}